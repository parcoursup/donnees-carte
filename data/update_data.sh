#!/bin/bash
ressourceUrl="https://ressource.parcoursup.fr/data"
rm files.xml
wget $ressourceUrl/files.xml
jsonFilename=$(xmllint --xpath '//campagneFilename/text()' files.xml)
zipFilename=${jsonFilename}.zip
rm ${zipFilename}
wget $ressourceUrl/${zipFilename}
rm ${jsonFilename}
unzip ${zipFilename}
rm psupdata.json
cp -f ${jsonFilename} psupdata.json

