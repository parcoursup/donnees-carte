
# update the data
run the script `update_data.sh` from within `data`

# load the data and print statistics
mvn exec:java -Dexec.mainClass="fr.gouv.parcoursup.carte.PrintStatistics"