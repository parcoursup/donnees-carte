package fr.gouv.parcoursup.carte.modele.modele;

import java.util.Comparator;
import java.util.Map;

public class ComparateurItems<K, V> implements Comparator<Map.Entry<K, V>> {

	public int compare(Map.Entry<K, V> entry1, Map.Entry<K, V> entry2) {
		Item i1 = (Item) entry1.getValue();
		Item i2 = (Item) entry2.getValue();
		
		if(i1 == null) return 1;
		if(i2 == null) return -1;

		/* Si les item on un ordre de tri et qu'il n'est pas egal on en tiens compte*/
		if (i1.getOrd() > 0 && i2.getOrd() > 0 && i1.getOrd() != i2.getOrd()) {
			return i1.getOrd() - i2.getOrd();		
		}else if (i1.getOrd() > 0 && i2.getOrd() == 0) {
			/*Si i1 a un ordre il est prioritaire */
			return -1;
		}else if (i1.getOrd() == 0 && i2.getOrd() > 0) {
			/*Si i1 a un ordre il est prioritaire */
			return 1;
		}else {
			/* Sinon on se base sur le nom */
			return i1.getNm().compareTo(i2.getNm());
		}
	}   
}